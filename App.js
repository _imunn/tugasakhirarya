import mainTabs from './src/navigation';
import { createAppContainer } from 'react-navigation';

const App = createAppContainer(mainTabs);
export default App;